package com.pzb.covid19.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@TableName("covid19_one_day")
public class Covid19OneDay {
    private static final long serialVersionUID = 1L;

    @TableId(value = "dateId", type = IdType.AUTO)
    private String dateId;

    @TableField("confirmedIncr")
    private Long confirmedincr;

    @TableField("confirmedCount")
    private Long confirmedcount;

    @TableField("suspectedCount")
    private Long suspectedcount;

    @TableField("curedCount")
    private Long curedcount;

    @TableField("deadCount")
    private Long deadcount;
}
