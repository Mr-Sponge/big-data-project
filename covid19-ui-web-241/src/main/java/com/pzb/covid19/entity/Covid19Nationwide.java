package com.pzb.covid19.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

@Data
@EqualsAndHashCode(callSuper = false)
@TableName("covid19_nationwide")
public class Covid19Nationwide implements Serializable {
    private static final long serialVersionUID = 1L;

    @TableId(value = "datetime", type = IdType.AUTO)
    private String datetime;

    @TableField("locationId")
    private Integer locationid;

    @TableField("provinceShortName")
    private String provinceshortname;

    @TableField("cityName")
    private String cityname;

    @TableField("currentConfirmedCount")
    private Integer currentconfirmedcount;

    @TableField("confirmedCount")
    private Integer confirmedcount;

    @TableField("suspectedCount")
    private Integer suspectedcount;

    @TableField("curedCount")
    private Integer curedcount;

    @TableField("deadCount")
    private Integer deadcount;

    private Integer pid;
}
