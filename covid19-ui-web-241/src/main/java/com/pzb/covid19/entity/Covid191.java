package com.pzb.covid19.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author pzb
 * @since 2022-06-29
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("covid191")
public class Covid191 implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "datetime", type = IdType.AUTO)
    private String datetime;

    @TableField("currentConfirmedCount")
    private Long currentconfirmedcount;

    @TableField("confirmedCount")
    private Long confirmedcount;

    @TableField("suspectedCount")
    private Long suspectedcount;

    @TableField("curedCount")
    private Long curedcount;

    @TableField("deadCount")
    private Long deadcount;


}
