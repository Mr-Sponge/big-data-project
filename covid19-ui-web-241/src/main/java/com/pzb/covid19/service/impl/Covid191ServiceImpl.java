package com.pzb.covid19.service.impl;

import com.pzb.covid19.entity.Covid191;
import com.pzb.covid19.mapper.Covid191Mapper;
import com.pzb.covid19.service.ICovid191Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author pzb
 * @since 2022-06-29
 */
@Service
public class Covid191ServiceImpl extends ServiceImpl<Covid191Mapper, Covid191> implements ICovid191Service {

}
