package com.pzb.covid19.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pzb.covid19.entity.Covid19OneDay;
import com.pzb.covid19.mapper.Covid19OneDayMapper;
import com.pzb.covid19.service.ICovid19OneDayService;
import org.springframework.stereotype.Service;

@Service
public class Covid19OneDayServiceImpl extends ServiceImpl<Covid19OneDayMapper, Covid19OneDay> implements ICovid19OneDayService {
}
