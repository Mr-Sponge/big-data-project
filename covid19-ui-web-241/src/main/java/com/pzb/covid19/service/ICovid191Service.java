package com.pzb.covid19.service;

import com.pzb.covid19.entity.Covid191;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author pzb
 * @since 2022-06-29
 */
public interface ICovid191Service extends IService<Covid191> {

}
