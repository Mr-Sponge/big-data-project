package com.pzb.covid19.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.pzb.covid19.entity.Covid19Nationwide;

public interface ICovid19NationwideService extends IService<Covid19Nationwide> {
}
