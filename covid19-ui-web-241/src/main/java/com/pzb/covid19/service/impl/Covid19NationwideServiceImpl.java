package com.pzb.covid19.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pzb.covid19.entity.Covid19Nationwide;
import com.pzb.covid19.mapper.Covid19NationwideMapper;
import com.pzb.covid19.service.ICovid19NationwideService;
import org.springframework.stereotype.Service;

@Service
public class Covid19NationwideServiceImpl extends ServiceImpl<Covid19NationwideMapper, Covid19Nationwide> implements ICovid19NationwideService {
}
