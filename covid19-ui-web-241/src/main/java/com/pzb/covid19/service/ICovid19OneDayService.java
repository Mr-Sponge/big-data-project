package com.pzb.covid19.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.pzb.covid19.entity.Covid19OneDay;

public interface ICovid19OneDayService extends IService<Covid19OneDay> {
}
