package com.pzb.covid19.service.impl;

import com.pzb.covid19.entity.Covid19Wz;
import com.pzb.covid19.mapper.Covid19WzMapper;
import com.pzb.covid19.service.ICovid19WzService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author pzb
 * @since 2022-06-29
 */
@Service
public class Covid19WzServiceImpl extends ServiceImpl<Covid19WzMapper, Covid19Wz> implements ICovid19WzService {

}
