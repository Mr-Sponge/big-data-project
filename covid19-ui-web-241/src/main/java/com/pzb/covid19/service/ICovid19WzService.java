package com.pzb.covid19.service;

import com.pzb.covid19.entity.Covid19Wz;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author pzb
 * @since 2022-06-29
 */
public interface ICovid19WzService extends IService<Covid19Wz> {

}
