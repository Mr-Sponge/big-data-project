package com.pzb.covid19.controller;

import com.alibaba.fastjson.JSON;
import com.pzb.covid19.entity.Covid191;
import com.pzb.covid19.entity.Covid19Nationwide;
import com.pzb.covid19.entity.Covid19OneDay;
import com.pzb.covid19.entity.Covid19Wz;
import com.pzb.covid19.service.ICovid191Service;
import com.pzb.covid19.service.ICovid19NationwideService;
import com.pzb.covid19.service.ICovid19OneDayService;
import com.pzb.covid19.service.ICovid19WzService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author pzb
 * @since 2022-06-29
 */
@Controller
@RequestMapping("/covid19")
public class Covid19Controller {
    @Autowired
    private ICovid19WzService iCovid19WzService;
    @RequestMapping("/covid19_wz")
    public String list(ModelMap modelMap){
        List<String> names = new ArrayList<>();
        List<Integer> cgs = new ArrayList<>();
        List<Integer> xbs = new ArrayList<>();
        List<Integer> jzs = new ArrayList<>();
        List<Integer> xhs = new ArrayList<>();
        List<Integer> xqs = new ArrayList<>();
        List<Integer> kcs = new ArrayList<>();
        for (Covid19Wz covid19Wz:iCovid19WzService.list()){
            names.add(covid19Wz.getName());
            cgs.add(covid19Wz.getCg());
            xbs.add(covid19Wz.getXb());
            jzs.add(covid19Wz.getJz());
            xhs.add(covid19Wz.getXh());
            xqs.add(covid19Wz.getXq());
            kcs.add(covid19Wz.getKc());

        }
        //转换成Json格式
        String nameJson = JSON.toJSONString(names);
        String cgJson = JSON.toJSONString(cgs);
        String xbJson = JSON.toJSONString(xbs);
        String jzJson = JSON.toJSONString(jzs);
        String xhJson = JSON.toJSONString(xhs);
        String xqJson = JSON.toJSONString(xqs);
        String kcJson = JSON.toJSONString(kcs);

        //将数据传到前端
        modelMap.put("nameJson",nameJson);
        modelMap.put("cgJson",cgJson);
        modelMap.put("xbJson",xbJson);
        modelMap.put("jzJson",jzJson);
        modelMap.put("xhJson",xhJson);
        modelMap.put("xqJson",xqJson);
        modelMap.put("kcJson",kcJson);

        return "covid19_wz";
    }


    //TODO 全国疫情趋势
    @Autowired
    private ICovid19OneDayService iCovid19OneDayService;
    @RequestMapping("/covid19_oneday")
    public String list3(ModelMap modelMap){
        List<String> dateIds = new ArrayList<>();
        List<Long> confirmedIncrs = new ArrayList<>();
        List<Long> confirmedCounts = new ArrayList<>();
        List<Long> suspectedCounts = new ArrayList<>();
        List<Long> curedCounts = new ArrayList<>();
        List<Long> deadCounts = new ArrayList<>();
        for (Covid19OneDay covid19OneDay : iCovid19OneDayService.list()){
            dateIds.add(covid19OneDay.getDateId());
            confirmedIncrs.add(covid19OneDay.getConfirmedincr());
            confirmedCounts.add(covid19OneDay.getConfirmedcount());
            suspectedCounts.add(covid19OneDay.getSuspectedcount());
            curedCounts.add(covid19OneDay.getCuredcount());
            deadCounts.add(covid19OneDay.getDeadcount());
        }


        modelMap.put("dateIdJson",dateIds);
        modelMap.put("confirmedIncrJson",confirmedIncrs);
        modelMap.put("confirmedCountJson",confirmedCounts);
        modelMap.put("suspectedCountJson",suspectedCounts);
        modelMap.put("curedCountJson",curedCounts);
        modelMap.put("deadCountJson",deadCounts);

        return "covid19_oneday";
    }

    //TODO 全国疫情情况
    @Autowired
    private ICovid19NationwideService iCovid19NationwideService;
    @RequestMapping("/covid19_nationwide")
    public String list4(ModelMap modelMap){
        //[{name:  ,value: }]

        List<Covid19Nationwide> datas = iCovid19NationwideService.list();

        List<Map<String,String>> mubiao= new ArrayList<Map<String,String>>();

        //provinceshortname   confirmedcount
        for (Covid19Nationwide t : datas){
            Map<String,String> d = new HashMap<>();
            d.put("name",t.getProvinceshortname());
            d.put("value",String.valueOf(t.getConfirmedcount()));
            mubiao.add(d);
        }

        modelMap.put("test",mubiao);

        return "covid19_nationwide";

    }
}
