package com.pzb.covid19.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pzb.covid19.entity.Covid19Nationwide;

public interface Covid19NationwideMapper extends BaseMapper<Covid19Nationwide> {
}
