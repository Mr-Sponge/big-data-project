package com.pzb.covid19.mapper;

import com.pzb.covid19.entity.Covid19Wz;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author pzb
 * @since 2022-06-29
 */
public interface Covid19WzMapper extends BaseMapper<Covid19Wz> {

}
