package com.pzb.covid19.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pzb.covid19.entity.Covid19OneDay;

public interface Covid19OneDayMapper extends BaseMapper<Covid19OneDay> {
}
