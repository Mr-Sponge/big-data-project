package com.pzb.covid19;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication//包括自动配置，组件扫描(包括bean,和包说明)和Spring Boot配置
@EnableScheduling//开启SpringBoot程序的定时任务调度
public class DatasourceApplication {

    public static void main(String[] args) {
        SpringApplication.run(DatasourceApplication.class, args);
    }

}

