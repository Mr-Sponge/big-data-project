package com.pzb.covid19.utils;

import org.apache.commons.lang3.time.FastDateFormat;

public abstract class TimeUtils {
    //用于获取当前时间戳
    public static String format(Long timestamp, String pattern){
        return FastDateFormat.getInstance(pattern).format(timestamp);
    }

    public static void main(String[] args){
        String format = TimeUtils.format(System.currentTimeMillis(),"yyyy-MM-dd");
        System.out.println(format);
    }
}
