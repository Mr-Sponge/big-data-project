# 新冠疫情数据实时可视化

#### 简介
基于SparkStreamming、Kafka、SpringBoot、MySQL、Mybatis的一个新冠疫情数据实时可视化项目。

#### 项目概述
用Java爬虫技术采集数据，将数据发送到kafka消息中间件；用Spark Streamming框架接收kafka消息，进行数据清洗，提炼出重要信息，然后将数据存储到Mysql关系型数据库中；用SpringBoot框架对Mysql里的数据进行提取，结合Echarts插件实现数据的不同可视化界面。


#### 结果展示

![输入图片说明](%E6%B1%82%E6%8F%B4%E7%89%A9%E8%B5%84.png)

![输入图片说明](%E5%9C%B0%E5%9B%BE%E6%95%B0%E6%8D%AE.png)

![输入图片说明](%E7%BB%9F%E8%AE%A1%E5%88%86%E6%9E%90.png)